// з колбеками
function createNewUser(askName, askLastName) {
    let newUser = {
        firstName: askName(),
        lastName: askLastName(),
        getLogin() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
    }

    Object.defineProperty(newUser, 'firstName', {
        value: this.firstName,
        writable: false,
        configurable: false,
    });

    Object.defineProperty(newUser, 'lastName', {
        value: this.lastName,
        writable: false,
        configurable: false,
    });
    
    //перевірка
    newUser.firstName = "xxx";
    newUser.lastName = "ooo";

    return newUser;
}

let result = createNewUser(askName, askLastName);

function askName() {
    do {
        firstName = prompt('Enter your name:');
    } while (!firstName);

    return firstName;
}
function askLastName() {
    do {
        lastName = prompt('Enter your lastname:');
    } while (!lastName);

    return lastName;
}

console.log(result);
console.log(result.getLogin());
console.log()

//або з сеттерами але без додаткового завдання


// function createNewUser(){
//     let newUser = {
//         get firstName(){
//             do {
//                 firstName = prompt('Enter your name:');
//             } while (!firstName);

//             return firstName;
//         },
//         get lastName(){
//             do {
//                 lastName = prompt('Enter your lastname:');
//             } while (!lastName);

//             return lastName;
//         },
//         getLogin(){
//            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
//         },
//         setFirstName(){

//         },
//         setLastName(){

//         },

//     };
//     return newUser;
// }

// let result = createNewUser();


// console.log(result);
// console.log(result.getLogin());





